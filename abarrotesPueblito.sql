create database  abarrotespueblito;
use  abarrotespueblito;

create table categoria(
idcategoria int not null primary key AUTO_INCREMENT,
nombre varchar(100));

create table producto(

idproducto int not null primary key AUTO_INCREMENT,
nombre varchar(100),
precio double,
fkcategoria int,
foreign key(fkcategoria) references categoria(idcategoria));


create table proveedor(
idproveedor int not null primary key AUTO_INCREMENT,
nombre varchar(100),
direccion varchar(100),
telefono varchar(10));

create table compras(
idcompra int not null primary key AUTO_INCREMENT,
fkproveedor int,
fkproducto int,
fechacompra varchar(30),
cantidad double,
foreign key(fkproveedor) references proveedor(idproveedor),
foreign key (fkproducto) references producto(idproducto));


insert into categoria values(null, 'Pan'),(null,'Fruta/Verdura'),(null,'Medicina'),(null, 'Golosinas');

insert into proveedor values(null, 'Mar�a Perez','Rosas Moreno 36','4741127718'),(null, 'Jos� L�pez','Lazaro Cardenas 25','4745698655'), 
(null,'Carmen Ram�rez','Progreso 37','8328369643');

select * from categoria;

select * from proveedor;
select * from producto;
insert into producto values(null, 'Sprit',10.00, 1);


insert into compras values(null, 1, 3,'2020-02-19',5);

insert into compras values(null, 2, 4,'2020-02-25',30);


insert into compras values(null, 3, 1,'2020-02-17',10);

select * from compras;

create view v_compra as
Select idcompra as 'Folio', fechacompra as 'fecha', proveedor.nombre as 'Proveedor', producto.nombre as 'Producto', cantidad as 'Cantidad'
from compras, proveedor, producto where compras.fkproveedor = proveedor.idproveedor and compras.fkproducto = producto.idproducto;

select * from v_compra;


drop procedure if exists insertarCompras;
create procedure insertarCompras(
in _idcompra int,
in _fkproveedor int,
in _fkproducto int, 
in _fechacompra varchar(30),
in _cantidad double)
begin
declare x int;
select count(*) from compras where cantidad = _cantidad into x;
if x =! null then
insert into compras values(null, _fkproveedor, _fkproducto, _fechacompra, _cantidad);
end if;
end;
 

call insertarCompras(null,3,1,'2020-02-13',10.80);

select * from compras;



select * from producto order by fkcategoria;

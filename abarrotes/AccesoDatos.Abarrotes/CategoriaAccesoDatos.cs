﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Abarrotes;
using System.Data;

namespace AccesoDatos.Abarrotes
{
    public class CategoriaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public CategoriaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "abarrotespueblito", 3306);
        }

        public void Guardar(Categorias categoria)
        {
            if (categoria.idcategoria == 0)
            {
                string consulta = string.Format("Insert into categoria values(null,'{0}')",
                categoria.nombre);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update categoria set nombre ='{0}'where idcategoria={1}",
                 categoria.nombre, categoria.idcategoria);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(int idcategoria)
        {
            string consulta = string.Format("Delete from categoria where idcategoria={0}", idcategoria);
            conexion.EjecutarConsulta(consulta);
        }

        public List<Categorias> GetCategorias(string filtro)
        {

            var listCategoria = new List<Categorias>();
            var ds = new DataSet();
            string consulta = "Select * from categoria where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "categoria");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var categoria = new Categorias
                {
                    idcategoria= Convert.ToInt32(row["idcategoria"]),
                   
                    nombre = row["nombre"].ToString()

                };

                listCategoria.Add(categoria);
            }

            return listCategoria;

        }


    }
}

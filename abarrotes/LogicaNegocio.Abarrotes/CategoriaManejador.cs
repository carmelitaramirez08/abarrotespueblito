﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Abarrotes;
using Entidades.Abarrotes;
using System.Text.RegularExpressions;
using System.Data;

namespace LogicaNegocio.Abarrotes
{
    public class CategoriaManejador
    {
        private CategoriaAccesoDatos _categoriaAccesoDatos = new CategoriaAccesoDatos();
    

    public void Guardar(Categorias categorias)
    {
        _categoriaAccesoDatos.Guardar(categorias);


        
    }
        public void Eliminar(int idcategoria)
        {
            _categoriaAccesoDatos.Eliminar(idcategoria);
        }

        public List<Categorias> GetCategorias(string filtro)
        {
            var listcategoria = _categoriaAccesoDatos.GetCategorias(filtro);
            return listcategoria;
        }


    }
}


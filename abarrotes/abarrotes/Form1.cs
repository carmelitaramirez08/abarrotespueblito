﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.Abarrotes;
using LogicaNegocio.Abarrotes;

namespace abarrotes
{
    public partial class Form1 : Form
    {
        private CategoriaManejador _categoriamanejador;
        private Categorias _categoria;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _categoriamanejador = new CategoriaManejador();
            _categoria = new Categorias();

            BuscarCategorias("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void CargarCategoria()
        {
            _categoria.idcategoria = Convert.ToInt32(lblId.Text);
            _categoria.nombre = txtNombre.Text;
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnAgregar.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;

        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
        }

        private void BuscarCategorias(string filtro)
        {
            dgvCategoria.DataSource = _categoriamanejador.GetCategorias(filtro);
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);

            txtNombre.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            CargarCategoria();
            if (true)
            {
                try
                {
                    GuardarCategoria();
                    LimpiarCuadros();
                    BuscarCategorias("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false);
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void GuardarCategoria()
        {
            _categoriamanejador.Guardar(_categoria);

        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarCategorias(txtBuscar.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    EliminarCategoria();
                    BuscarCategorias("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarCategoria()
        {
            var idcategoria = dgvCategoria.CurrentRow.Cells["idcategoria"].Value;
            _categoriamanejador.Eliminar(Convert.ToInt32(idcategoria));
        }
        private  void ModificarCategoria()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dgvCategoria.CurrentRow.Cells["idcategoria"].Value.ToString();
            txtNombre.Text = dgvCategoria.CurrentRow.Cells["nombre"].Value.ToString();
        }

        private void DgvCategoria_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarCategoria();
            BuscarCategorias("");
        }
    }
}
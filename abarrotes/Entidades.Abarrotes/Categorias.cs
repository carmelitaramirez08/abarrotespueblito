﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Abarrotes
{
    public class Categorias
    {
        private int _idcategoria;
        private string _nombre;

        public int idcategoria { get => _idcategoria; set => _idcategoria = value; }
        public string nombre { get => _nombre; set => _nombre = value; }
    }
}
